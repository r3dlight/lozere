#[cfg(test)]
use super::*;

#[cfg(test)]
//mod[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_to_hex_string() {
        let bytes: Vec<u8> = vec![0xFF, 0, 0xAA];
        let actual = to_hex_string(bytes);
        assert_eq!("FF00AA", actual);
    }
}
