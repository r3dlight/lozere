#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

//use sha2::{Digest, Sha256};
//use std::fs::File;
//use async_std::fs::File;
use async_std::fs;
use async_std::prelude::*;
use colored::*;
use std::path::Path;
use nix::unistd::{chown, User};
//mod test_mod;

pub use anyhow::Result;

pub struct FileUploaded {
    pub name: String,
    /*uid: u32,
    gid: i32,*/
    pub dir: bool,
    pub len: u64,
    pub perm: std::fs::Permissions,
    //pub hash: Vec<u8>,
}

pub trait Utils {
    //fn get_hash(&self) -> Vec<u8>;
    fn get_name(&self) -> String;
    fn is_dir(&self) -> String;
    fn get_len(&self) -> String;
    fn get_perm(&self) -> std::fs::Permissions;
}

impl Utils for FileUploaded {
    //fn get_hash(&self) -> Vec<u8> {
    //    self.hash.clone()
    //}
    fn is_dir(&self) -> String {
        self.dir.to_string()
    }
    fn get_len(&self) -> String {
        self.len.to_string()
    }
    fn get_name(&self) -> String {
        self.name.to_string()
    }
    fn get_perm(&self) -> std::fs::Permissions {
        self.perm.clone()
    }
}

/// returns a sha512 digest for a file
//pub fn hashme(file: &str) -> Result<Vec<u8>> {
//    let mut open_file = File::open(file)?;
//    let mut sha256: Sha256 = Sha256::new();
//    std::io::copy(&mut open_file, &mut sha256)?;
//    let hash = sha256.result();
//    Ok(hash.as_slice().to_owned())
//}

// Get the Vec<u8> sha512 digest and return a String
//pub fn to_hex_string(bytes: Vec<u8>) -> String {
//    let strs: Vec<String> = bytes.iter().map(|b| format!("{:02X}", b)).collect();
//    strs.join("")
//}

/// Remove a file or a named pipe
pub async fn remove(file: &str) -> Result<()> {
    fs::remove_file(file).await?;
    debug!("Removing file : {}", file);
    Ok(())
}

/// Remove a directory
pub async fn remove_directory(dir: &str) -> Result<()> {
    fs::remove_dir_all(dir).await?;
    debug!("Removing directory : {}", dir);
    Ok(())
}

pub async fn move_files<'a>(outgoing: &'a str, incoming: &'a str, user: &'a str) -> Result<()> {
    let mut dir = fs::read_dir(incoming).await?;

    while let Some(entry) = dir.next().await {
        let entry = entry?;
        println!("{:?}", entry.file_name());

        let path_to_read_from = format!(
            "{}{}",
            incoming,
            entry.file_name().to_string_lossy().into_owned()
        );

        if fs::metadata(&path_to_read_from).await?.is_dir() {
            remove_directory(&path_to_read_from).await?;
            warn!(
                "Removing unwanted directory {} !",
                path_to_read_from.yellow()
            );
            break;
        }
        let file = FileUploaded {
            name: entry.file_name().to_string_lossy().into_owned(),
            perm: fs::metadata(&path_to_read_from).await?.permissions(),
            dir: fs::metadata(&path_to_read_from).await?.is_dir(),
            len: fs::metadata(&path_to_read_from).await?.len(),
            //hash: hashme(&path_to_read_from)?,
        };
        info!("Filename is: {}", file.get_name().green());
        info!("File permissions: {:?}", file.get_perm());
        info!("Is a directory: {}", file.is_dir().green());
        info!("File lengh: {}", file.get_len().green());
        //info!("Sha256 is: {}", to_hex_string(file.get_hash()).green());

        let path_to_write = format!(
            "{}{}",
            outgoing,
            entry.file_name().to_string_lossy().into_owned()
        );
        let path_to_read_from = format!(
            "{}{}",
            incoming,
            entry.file_name().to_string_lossy().into_owned()
        );
        warn!("Reading from: {}", path_to_read_from);
        warn!("Writing to: {}", path_to_write);
        fs::copy(&path_to_read_from, &path_to_write).await?;
        let user = User::from_name(user).unwrap().unwrap();
        chown(Path::new(&path_to_write), Some(user.uid), Some(user.gid)).unwrap();
        //assert_eq!(file.hash, hashme(&path_to_write)?);
        remove(&path_to_read_from).await?;
    }
    Ok(())
}
