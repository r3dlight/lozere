#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(missing_debug_implementations)]
#![warn(missing_copy_implementations)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

use async_std::task;
use gumdrop::Options;
use std::thread as main_thread;
use std::time::Duration;
//use async_std::fs::File;
//use async_std::fs;
//use std::path::{Path, PathBuf};
//use async_std::prelude::*;
#[macro_use]
extern crate log;
use colored::*;
use flexi_logger::{opt_format, Cleanup, Criterion, Duplicate, FileSpec, Logger, Naming};
pub use anyhow::Result;

mod lozere_utils;
use crate::lozere_utils::*;

#[derive(Debug, Options)]
struct MyArgs {
    // The optional `help` attribute is displayed in `usage` text.
    #[options(help = "print help message")]
    help: bool,

    #[options(help = "Give the max size of the current log file before rotating (u64).")]
    rotate_over_size: u64,

    #[options(help = "Give the path where files are coming from (String).")]
    incoming: String,

    #[options(help = "Give the path where files are going to be moved to (String).")]
    outgoing: String,

    #[options(help = "Give a path for the log directory (String).")]
    log_path: String,

    #[options(help = "Give the max number of log files to keep (usize).")]
    nb_of_log_files: usize,

    #[options(help = "The user name to change owner to. (String).")]
    user: String,
}

fn main() -> Result<()> {
    let args = MyArgs::parse_args_default_or_exit();

    Logger::try_with_env_or_str("warn")?
        .log_to_file(FileSpec::default().directory(&args.log_path))
        .format(opt_format)
        .duplicate_to_stderr(Duplicate::Info)
        .rotate(
            Criterion::Size(args.rotate_over_size),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(args.nb_of_log_files),
        )
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    info!("{}", "Lozere is starting".green());
    info!("Watching directory for files...");
    info!("Incoming Path : {}", &args.incoming.green());
    info!("Outgoing directory: {}", &args.outgoing.green());
    info!("Log Path : {}", &args.log_path.green());
    info!("Rotate logs over size : {:?}", args.rotate_over_size);
    info!("Number of logs to keep : {:?}", args.nb_of_log_files);
    info!("User name for chown : {:?}", args.user);

    loop {
        //Each arg must be cloned because they are moved on each async thread spawn
        let outgoing = args.outgoing.clone();
        let incoming = args.incoming.clone();
        let user = args.user.clone();
        let copy_task = task::spawn(async move {
            let result = move_files(&outgoing, &incoming, &user).await;
            match result {
                Ok(_s) => (),
                Err(e) => error!("Error moving file: {:?}", e),
            }
        });
        task::block_on(copy_task);
        //Sleep a bit to save CPU
        main_thread::sleep(Duration::from_millis(120));
    }
}
