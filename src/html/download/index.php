<?php

require_once('../config.php');

if (!$_SESSION['userid'])
{
	header('Location: ../login.php');
	die();
}

if ($connected != 1)
{
	//echo "You are not authorized to access this file.";
	//die();
}

if($_GET['file'] != '')
{
	$invalid_characters = array('$', '|', '`'); // $, | and quote variation not escaped by htmlspecialschars
	$filename = str_replace($invalid_characters, "", stripslashes($_GET['file']));
	$filename = htmlspecialchars($filename, ENT_QUOTES);
	$filename = basename($filename);
	$clean_filename = $filename;
	$filename = $_SESSION['userid'] . '_' . $filename; // add user id prefix to re-create original file name, as filenames are passed without to look clean
	
	$local_filepath = '../'.$downloadBaseURI . $filename;
	
	$extension=pathinfo($filename)['extension'];
	
	if($extension !='php' && $extension !='htaccess') // prevent getting php or htaccess source code
	{
		// check if the file exist before
		if(is_file($local_filepath))
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$clean_filename.'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($local_filepath));
			@readfile($local_filepath); // Read and send file
		}
		
	}
	
}

?>