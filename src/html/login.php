<?php

require_once('./config.php');

if ($_SESSION['userid'])
{
	header('Location: ./');
	die();
}

// id|username|pass(SHA512)|role(admin/user)
// ...

if(isset($_POST['username']) && isset($_POST['password']))
{
	$username = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['username']);
	$password = hash('sha256', $_POST['password']);
	$userList = file($usersFile);

	$success = false;
	foreach ($userList as $user) {
		$user_details = explode('|', $user);
		if ($user_details[1] == $username && $user_details[2] == $password) {
			$success = true;
			$userid = $user_details[0];
			$username = $user_details[1];
			$role = $user_details[3];
			break;
		}
	}

	if ($success)
	{
		$_SESSION['userid'] = $userid;
		$_SESSION['username'] = $username;
		$_SESSION['role'] = $role;
		$_SESSION['snackmsg'] = "Snackbar.show({text: 'Successfully logged in as $username.', pos: 'bottom-center', showAction: false});";
		header('Location: ./');
		die();
	}
	
	if(!$success)
	{
		$_SESSION['snackmsg'] = "Snackbar.show({text: 'You have entered the wrong username or password. Please try again.', pos: 'bottom-center', showAction: false});";
	}
	
}
	





?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="./img/favicon.png">
		<!-- Meta Description -->
		<meta name="description" content="">
		
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>LeGuichet</title>
		
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/filepond.css">
		<!-- SnackBarJS CSS -->
		<link rel="stylesheet" href="css/snackbar.min.css">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/custom.css">
	</head>
	<body>
		<!-- Start Header Area -->
		<header class="default-header">
			<div class="container">
				<div class="header-wrap">
					<div class="header-top d-flex justify-content-between align-items-center">
						<div class="logo">
							<a href="./"><img src="./img/logo-white.png" width="150" alt=""></a>
						</div>
						<div class="main-menubar d-flex align-items-center">
							<nav class="hide">
								<a href="./">Demo</a>
								<a href="#about">About</a>
								<a href="#contact">Contact</a>
							</nav>
							<div class="menu-bar"><span class="lnr lnr-menu"></span></div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header Area -->
		<!-- Start Banner Area -->
		<section class="generic-banner relative">
			<div class="container" style="min-height:550px;">
				<div class="row align-items-center justify-content-center" style="padding-top:150px;">
					<div class="col-lg-10">
						<div class="banner-content text-center">
							<h2 class="text-white">LeGuichet | Login</h2>
							<p class="text-white">Please login to continue.</p>
						</div>
					</div>
				</div>
				<div class="row align-items-center justify-content-center" style="padding-top:50px;">
					<div class="col-lg-4">
						<form method="post">
							<span class="black">Username</span><input class="single-input" type="text" name="username" style="margin-bottom: 15px;">
							<span class="black">Password</span><input class="single-input" type="password" name="password">
							<br>
							<div class="row align-items-center justify-content-center">
								<div class="col-lg-6 text-center">
									<button type="submit" class="genric-btn primary-border circle arrow">Login</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="row align-items-center justify-content-center" style="padding-top:30px;">
					<div class="col-lg-8">
						
						<div id="queue">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
						
						<div id="available">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
					
					
					</div>
				</div>
			</div>
		</section>
		<!-- End Banner Area -->

		<!-- Start feature Area -->
		<section class="feature-area">
			<div class="container">
				<div class="row pt-100">
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>01</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">You send untrusted files using a simple web browser</h2>
								<p>
								Files are sent to the server and moved into an input folder.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>02</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">One-way malware analysis is performed</h2>
								<p>
								The files pass through a software diode. They are analyzed one by one in a static and dynamic way.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>03</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">
									Clean files are released
								</h2>
								<p>
								Uninfected files are made available in an output folder. A Yara report is available for each file.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End feature Area -->

		<!-- Start video Area -->
		<section id="about">
			<div class="container">
				<div class="row d-flex justify-content-center video-wrap">
					<div class="col-lg-8 col-md-12">
						<div class="content-wrap pt-60 pb-60">
							<h2><span>About</span></h2>
							<br>
							<h5>Conception & Backend Core</h5>
							<p class="black">Stéphane Neveu</p>
							<h5>Web Frontend & PHP Backend</h5>
							<p class="black">Edouard Lamoine</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End video Area -->
		<!-- About Generic Start -->
		<section id="contact" class="about-generic-area">
			<div class="container border-top-generic">
				<div class="row d-flex justify-content-center">
					<div class="col-lg-8 col-md-12">
						<h2><span>Contact</span></h2>
						<br>
						<img class="img-responsive mx-auto d-block" src="./img/logo.png" style="width:50%;">
						
					</div>
					
				</div>
			</div>
		</section>
		<!-- End Generic Start -->
		
		<!-- Start Footer Area -->
		<footer class="footer-area pt-40 pb-40">
			<div class="container">
				<div class="footer-bottom justify-content-between text-center">
					<p class="footer-text m-0 text-white">Copyright &copy;<script>document.write(new Date().getFullYear());</script> LeGuichet, All rights reserved</p>
				</div>
			</div>
		</footer>
		<!-- End Footer Area -->
		
    <!-- include jQuery library -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="js/bootstrap.bundle.js"></script>
	
	<!-- include menu JS -->
	<script src="js/menu.js"></script>
	
	<!-- include SnackBarJS -->
	<script src="js/snackbar.min.js"></script>
	
	
	<script>
	  $(function(){
		  
		// Snack Message //  
		
		<?php
		if($_SESSION['snackmsg'] != '')
		{
			echo $_SESSION['snackmsg'];
			$_SESSION['snackmsg'] = '';
		}
		?>
		
		// Snack Message end //
	  });
	</script>
	
	
	</body>
</html>
