<?php

require_once('./config.php');

if (!$_SESSION['userid'])
{
	header('Location: ./login.php');
	die();
}

?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="./img/favicon.png">
		<!-- Meta Description -->
		<meta name="description" content="">
		
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Le Guichet</title>
		
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/filepond.css">
		<!-- SnackBarJS CSS -->
		<link rel="stylesheet" href="css/snackbar.min.css">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/custom.css">
	</head>
	<body>
		<!-- Start Header Area -->
		<header class="default-header">
			<div class="container">
				<div class="header-wrap">
					<div class="header-top d-flex justify-content-between align-items-center">
						<div class="logo">
							<a href="./"><img src="./img/logo-white.png" width="150" alt=""></a>
						</div>
						<div class="main-menubar d-flex align-items-center">
							<nav class="hide">
								<a href="./">Demo</a>
								<a href="#about">About</a>
								<a href="#contact">Contact</a>
								<a href="?action=logout">Logout (<?php echo $_SESSION['username']; ?>)</a>
							</nav>
							<div class="menu-bar"><span class="lnr lnr-menu"></span></div>
							
							
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header Area -->
		<!-- Start Banner Area -->
		<section class="generic-banner relative">
			<div class="container" style="min-height:550px;">
				<div class="row align-items-center justify-content-center" style="padding-top:150px;">
					<div class="col-lg-10">
						<div class="banner-content text-center">
							<h2 class="text-white">LeGuichet</h2>
							<p class="text-white">Hello <?php echo $_SESSION['username']; ?>, send your files here, they will be processed. <br>Overall progress can also be tracked on this page.</p>
							<?php if($_SESSION['role'] == 'admin'){echo '<br><p class="text-white">Customize users here: <u><a class="text-light" href="admin.php">Admin panel</a></u></p>';} ?>
						</div>
					</div>
				</div>
				<div class="row align-items-center justify-content-center" style="padding-top:50px;">
					<div class="col-lg-6">
						<form action="filepond/submit.php" method="post" enctype="multipart/form-data">

							<input type="file" name="filepond[]" multiple>
							
							<div class="row align-items-center justify-content-center">
								<div class="col-lg-6 text-center">
									<button id="send" type="submit" class="genric-btn primary-border circle arrow" style="visibility:hidden; opacity:0;" disabled="yes">Send & Process<span class="lnr lnr-arrow-right"></span></button>
								</div>
							</div>
						
						</form>
						
					</div>
				</div>
				
				<div class="row align-items-center justify-content-center" style="padding-top:30px;">
					<div class="col-lg-8">
						
						<div id="queue">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
						
						<div id="available">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
					
					
					</div>
				</div>
			</div>
		</section>
		<!-- End Banner Area -->

		<!-- Start feature Area -->
		<section class="feature-area">
			<div class="container">
				<div class="row pt-100">
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>01</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">You send untrusted files using a simple web browser</h2>
								<p>
								Files are sent to the server and moved into an input folder.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>02</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">One-way malware analysis is performed</h2>
								<p>
								The files pass through several diodes. They are analyzed one by one by Clamav antivirus and Yara.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>03</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">
									Clean files are released
								</h2>
								<p>
								Uninfected files are made available in an output folder. A Yara report is created if a rule matches.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End feature Area -->

		<!-- Start video Area -->
		<section id="about">
			<div class="container">
				<div class="row d-flex justify-content-center video-wrap">
					<div class="col-lg-8 col-md-12">
						<div class="content-wrap pt-60 pb-60">
							<h2><span>About</span></h2>
							<br>
							<h5>Conception & Backend Core</h5>
							<p class="black">Stéphane Neveu</p>
							<h5>Web Frontend & PHP Backend</h5>
							<p class="black">Edouard Lamoine</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End video Area -->
		<!-- About Generic Start -->
		<section id="contact" class="about-generic-area">
			<div class="container border-top-generic">
				<div class="row d-flex justify-content-center">
					<div class="col-lg-8 col-md-12">
						<h2><span>Contact</span></h2>
						<br>
						<img class="img-responsive mx-auto d-block" src="./img/logo.png" style="width:50%;">
						
					</div>
					
				</div>
			</div>
		</section>
		<!-- End Generic Start -->
		
		<!-- Start Footer Area -->
		<footer class="footer-area pt-40 pb-40">
			<div class="container">
				<div class="footer-bottom justify-content-between text-center">
					<p class="footer-text m-0 text-white">Copyright &copy;<script>document.write(new Date().getFullYear());</script> LeGuichet, All rights reserved</p>
				</div>
			</div>
		</footer>
		<!-- End Footer Area -->
		
    <!-- include jQuery library -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="js/bootstrap.bundle.js"></script>
	
	<!-- include menu JS -->
	<script src="js/menu.js"></script>
  
	<!-- include FilePond plugins -->
	<script src="js/filepond-plugin-file-validate-size.js"></script>
	
	<!-- include FilePond library -->
	<script src="js/filepond.min.js"></script>	

	<!-- include FilePond jQuery adapter -->
	<script src="js/filepond.jquery.js"></script>
	
	<!-- include SnackBarJS -->
	<script src="js/snackbar.min.js"></script>
	
	
	<script>
  $(function(){
	  
	// Snack Message //  
	
	<?php
	if($_SESSION['snackmsg'] != '')
	{
		echo $_SESSION['snackmsg'];
		$_SESSION['snackmsg'] = '';
	}
	?>
	
	// Snack Message end //
  
	
	// FilePond //
	
    // First register any plugins
    FilePond.registerPlugin(FilePondPluginFileValidateSize);

	// Set default FilePond options
    FilePond.setOptions({
        // maximum allowed file size
        maxFileSize: '64MB',
        // upload to this server end point
        server: 'filepond/'
    });
	
	 // Turn input element into a pond
	var mypond = FilePond.create(document.querySelector('input[type="file"]'));
	

    // Listen for addfile event
    //mypond.onaddfile( function(e) {
      //  console.log('file added event', e);
    //});
	
    // Manually add a file using the addfile method
    //mypond.addFile('filename.xyz').then(function(file){
     // console.log('file added', file);
   // });
	
	
	// FilePond end //
	
	
	
	// auto refresh send button state //
	
	function refreshsendbuttonstate() // Will be reused at auto refresh
	{
		if(mypond.status == 4)
		{
			$("#send").attr("disabled", false);
			$('#send').css('visibility', 'visible').animate({opacity: 1.0});
		}
		else
		{
			$("#send").attr("disabled", true);
			$('#send').css('visibility', 'hidden');
		}
	}
	
	window.setInterval(function(){

	refreshsendbuttonstate();
		
	}, 100);	
	
	// end auto refresh send button state //
	
	
	// auto refresh pending files //
	
	var lastQueue = '';
	getqueuehtml();
	
	function getqueuehtml() // Will be reused at auto refresh
	{
		var xhr = new XMLHttpRequest();
		var url = 'queue-checker-api.php?data=pending';
		xhr.open('GET', url, true);
		xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send();


		xhr.onreadystatechange = function ()
		{

			if (xhr.readyState === 4 && xhr.status === 200)
			{
				if( lastQueue != xhr.responseText)
				{
					lastQueue = xhr.responseText;
					$( '#queue' ).html(xhr.responseText);
					
					$(window).triggerHandler("resize"); // Retrigger tabs after appending it
					
				}
			
			}

		};
	}
		
		
		
	window.setInterval(function(){

	getqueuehtml();
		
	}, 5000);	
	
	// auto refresh pending files end //
	
	
	// auto refresh available to download files //
	
	var lastAvailable = '';
	getavailablehtml();
	
	function getavailablehtml() // Will be reused at auto refresh
	{
		var xhr = new XMLHttpRequest();
		var url = 'queue-checker-api.php?data=processed';
		xhr.open('GET', url, true);
		xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send();


		xhr.onreadystatechange = function ()
		{

			if (xhr.readyState === 4 && xhr.status === 200)
			{
				if( lastAvailable != xhr.responseText)
				{
					lastAvailable = xhr.responseText;
					$( '#available' ).html(xhr.responseText);
					
					$(window).triggerHandler("resize"); // Retrigger tabs after appending it
					
				}
			
			}

		};
	}
		
		
		
	window.setInterval(function(){

	getavailablehtml();
		
	}, 5000);	
	
	// auto refresh available to download files end //
	
	

  
});
</script>
	
	
	</body>
</html>
