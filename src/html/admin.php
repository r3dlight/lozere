<?php

require_once('./config.php');

if (!$_SESSION['userid'])
{
	header('Location: ./login.php');
	die();
}

if ($_SESSION['role'] != 'admin')
{
	echo 'Unauthorized.';
	die();
}

// id|username|pass(SHA512)|role(admin/user)
// ...


if(isset($_POST['delete']) && is_numeric($_POST['delete']))
{
	$deleteid = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['delete']);
	$userList = file($usersFile);

	$newContent = "";
	
	foreach ($userList as $user) {
		$user_details = explode('|', $user);
		$currentid = $user_details[0];
		
		if($currentid != $deleteid)
		{
			$newContent .= $user;
		}
		
	}
	
	$newContent=rtrim($newContent); // prevent blank new line at end of file when deleting the last user
	file_put_contents($usersFile, $newContent); // Rewrite users.txt without deleted user
	
	foreach (glob($downloadBaseURI . $deleteid . '_*') as $filename) // delete every file owned by the user ( starting by userid_ )
	{
		unlink($filename);
	}
	
	foreach (glob($uploadBaseURI . $deleteid . '_*') as $filename) // delete every file owned by the user ( starting by userid_ )
	{
		unlink($filename);
	}

	$_SESSION['snackmsg'] = "Snackbar.show({text: 'Request processed', pos: 'bottom-center', showAction: false});";
	
	
	if($deleteid == $_SESSION['userid']) // if self-deleted => logout
	{
		session_destroy();
		header('Location: ./');
	}

	
}

if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['role']))
{
	$newusername = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['username']);
	$newpassword = hash('sha256', $_POST['password']);
	$newrole = preg_replace("/[^A-Za-z0-9 ]/", '', $_POST['role']);
	$userList = file($usersFile);

	$donotinsert=0;
	
	foreach ($userList as $user) {
		$user_details = explode('|', $user);
		$lastid = $user_details[0];
		
		if($newusername == $user_details[1])
		{
			$donotinsert=1;
			$_SESSION['snackmsg'] = "Snackbar.show({text: 'Error: username already exists.', pos: 'bottom-center', showAction: false});";
			break;
		}
		
	}
	
	if($donotinsert==0)
	{
		$newUserString = "\n" . ($lastid+1) . '|' . $newusername . '|' . $newpassword . '|' . $newrole . '|';
		file_put_contents($usersFile, $newUserString, FILE_APPEND | LOCK_EX);
		$_SESSION['snackmsg'] = "Snackbar.show({text: 'Request processed', pos: 'bottom-center', showAction: false});";
	}
	
	
}
	



	





?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="./img/favicon.png">
		<!-- Meta Description -->
		<meta name="description" content="">
		
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>LeGuichet</title>
		
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/filepond.css">
		<!-- SnackBarJS CSS -->
		<link rel="stylesheet" href="css/snackbar.min.css">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/custom.css">
	</head>
	<body>
		<!-- Start Header Area -->
		<header class="default-header">
			<div class="container">
				<div class="header-wrap">
					<div class="header-top d-flex justify-content-between align-items-center">
						<div class="logo">
							<a href="./"><img src="./img/logo-white.png" width="150" alt=""></a>
						</div>
						<div class="main-menubar d-flex align-items-center">
							<nav class="hide">
								<a href="./">Demo</a>
								<a href="#about">About</a>
								<a href="#contact">Contact</a>
								<a href="?action=logout">Logout (<?php echo $_SESSION['username']; ?>)</a>
							</nav>
							<div class="menu-bar"><span class="lnr lnr-menu"></span></div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header Area -->
		<!-- Start Banner Area -->
		<section class="generic-banner relative">
			<div class="container" style="min-height:550px;">
				<div class="row align-items-center justify-content-center" style="padding-top:150px;">
					<div class="col-lg-10">
						<div class="banner-content text-center">
							<h2 class="text-white">LeGuichet | Admin panel</h2>
							<p class="text-white">Create or revoke users here. Revoking a user will also delete his files.</p>
							<br><p class="text-white"><u><a class="text-light" href="./">Back to files</a></u></p>
						</div>
					</div>
				</div>
				<div class="row align-items-center justify-content-center" style="padding-top:50px;">
					<div class="col-lg6">
						<table class="table table-striped" style="background-color: white;">
							<thead>
							  <tr>
								<th>Username</th>
								<th>Role</th>
								<th>Delete</th>
							  </tr>
							</thead>
							<tbody>
								<?php
									$userlist = file ('users.txt');
									foreach ($userlist as $user)
									{
										$user_details = explode('|', $user);
										echo '<tr>';
											echo '<td>' . $user_details[1] . '</td>';
											echo '<td>' . $user_details[3] . '</td>';
											echo '<td><form method="post"><input type="hidden" name="delete" value="'.$user_details[0].'"><input type="submit" class="text-danger" value="Delete"></form></td>';
										echo '</tr>';
									}
								?>
							</tbody>
						  </table>
						</div>
						<div class="col-lg-12">
						  <table class="table table-striped" style="background-color: white; margin-top: 50px;">
							<thead>
							  <tr>
								<th>Username</th>
								<th>Role</th>
								<th>Password</th>
								<th>Create</th>
							  </tr>
							</thead>
							<tbody>
								<tr>
								<form method="post">
									<td style="max-width: 200px;"><input style="max-width: 200px;" type="text" id="username" name="username"></td>
									<td>
										<label class="radio-inline"><input type="radio" id="user" name="role" value="user" checked> User</label>
										<label class="radio-inline"><input type="radio" id="admin" name="role" value="admin"> Admin</label>
									</td>
									<td style="max-width: 200px;"><input style="max-width: 200px;" type="password" id="password" name="password"></td>
									<td><input type="submit" value="Create"></td>
									</form>
								</tr>
							</tbody>
						  </table>
					</div>
				</div>
				
				<div class="row align-items-center justify-content-center" style="padding-top:30px;">
					<div class="col-lg-8">
						
						<div id="queue">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
						
						<div id="available">
																
						<!-- Eventual queue Listing populated by JS+API-->
															
						</div>
					
					
					</div>
				</div>
			</div>
		</section>
		<!-- End Banner Area -->

		<!-- Start feature Area -->
		<section class="feature-area">
			<div class="container">
				<div class="row pt-100">
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>01</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">You send untrusted files using a simple web browser</h2>
								<p>
								Files are sent to the server and moved into an input folder.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>02</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">One-way malware analysis is performed</h2>
								<p>
								The files pass through several software diodes. They are analyzed one by one by Clamav antivirus and Yara.
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="single-feature d-flex justify-content-between align-items-top">
							<div class="number">
								<h1>03</h1>
							</div>
							<div class="desc">
								<h2 class="text-uppercase">
									Clean files are released
								</h2>
								<p>
								Uninfected files are made available in an output folder. A Yara report is created if a rule matches.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End feature Area -->

		<!-- Start video Area -->
		<section id="about">
			<div class="container">
				<div class="row d-flex justify-content-center video-wrap">
					<div class="col-lg-8 col-md-12">
						<div class="content-wrap pt-60 pb-60">
							<h2><span>About</span></h2>
							<br>
							<h5>Conception & Backend Core</h5>
							<p class="black">Stéphane Neveu</p>
							<h5>Web Frontend & PHP Backend</h5>
							<p class="black">Edouard Lamoine</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End video Area -->
		<!-- About Generic Start -->
		<section id="contact" class="about-generic-area">
			<div class="container border-top-generic">
				<div class="row d-flex justify-content-center">
					<div class="col-lg-8 col-md-12">
						<h2><span>Contact</span></h2>
						<br>
						<img class="img-responsive mx-auto d-block" src="./img/logo.png" style="width:50%;">
						
					</div>
					
				</div>
			</div>
		</section>
		<!-- End Generic Start -->
		
		<!-- Start Footer Area -->
		<footer class="footer-area pt-40 pb-40">
			<div class="container">
				<div class="footer-bottom justify-content-between text-center">
					<p class="footer-text m-0 text-white">Copyright &copy;<script>document.write(new Date().getFullYear());</script> LeGuichet, All rights reserved</p>
				</div>
			</div>
		</footer>
		<!-- End Footer Area -->
		
    <!-- include jQuery library -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="js/bootstrap.bundle.js"></script>
	
	<!-- include menu JS -->
	<script src="js/menu.js"></script>
	
	<!-- include SnackBarJS -->
	<script src="js/snackbar.min.js"></script>
	
	
	<script>
	  $(function(){
		  
		// Snack Message //  
		
		<?php
		if($_SESSION['snackmsg'] != '')
		{
			echo $_SESSION['snackmsg'];
			$_SESSION['snackmsg'] = '';
		}
		?>
		
		// Snack Message end //
	  });
	</script>
	
	
	</body>
</html>
