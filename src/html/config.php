<?php

error_reporting(0);
session_start();

// Warning ! //
// Below configurations do not apply to Filepond (the upload file processor), but only to Leguichet web. Please make sure these are the same than Filepond configurations. //
//
// FAQ //
//
// Why arent these config files merged? So we have to edit only one config file? 
//	=> Once merged, have fun updating Filepond :)
//
// Downloading file give Server Unreachable error ?
// 	=> HTTPS is not enabled. Enable it or edit $downloadBaseURL

$usersFile = 'users.txt';
$uploadBaseURI = 'filepond/in/';
$downloadBaseURI = 'filepond/out/';
$downloadBaseURL = 'http://' . '127.0.0.1' . '/download/?file=';
//$downloadBaseURL = 'http://' . $_SERVER['SERVER_NAME'] . '/leguichet/download/?file=';

// logout 
if( $_GET['action'] == 'logout')
{
	session_destroy();
	header('Location: ./');
}
	

?>
