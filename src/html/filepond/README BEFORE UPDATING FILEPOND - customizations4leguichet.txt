If filePond update is needed, changes will have to be made again in following elements:

1. Improvements to avoid "filetest" being uploaded as "filetest."
	=> changing FilePond.class.php lines 228 & 232 to wildcard "*" instead of "*.*"
	=> changing FilePond.class.php lines 42: function modified to allow files without extensions without forcing the dot:
	******************************************************************************
	function sanitize_filename($filename) {
		$info = pathinfo($filename);
		$name = sanitize_filename_part($info['filename']);
		$extension = sanitize_filename_part($info['extension']);
		if($extension != '')
		{
			$extension = '.' . $extension;
		}
		
		return (strlen($name) > 0 ? $name : '_') . $extension;
	}
	******************************************************************************
	
2. LeGuichet file upload handled by send button + session handling
	=> changing submit.php: custom code at top & bottom of file
	
3. Leguichet file-user association system (know which file is owned by which user)
	=> adding a line on submit.class.php line 90: $file['name'] = $_SESSION['userid'].'_'.$file['name'];