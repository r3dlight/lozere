<?php
require_once('./config.php');



if($_GET["data"] === 'pending')
{
	$entries = 0;
	$entrieshtml = '';
	$user_prefix = $_SESSION['userid'] . '_';
	
	if ($handle = opendir($uploadBaseURI))
	{
		while (false !== ($entry = readdir($handle))) {

			if ($entry != "." && $entry != ".." && $entry != ".htaccess" && substr($entry, 0, strlen($user_prefix)) === $user_prefix)
			{
				$entries++;
				$entry = substr($entry, strlen($user_prefix));
				$entrieshtml.='<li><i style="margin-right:4px;" class="fa fa-circle-o-notch fa-spin fa-fw"></i>'.$entry.'</li>';
			}
		}

		closedir($handle);
	}
	
	if($entries > 0)
	{
		echo '<blockquote class="generic-blockquote" style="margin-bottom: 50px;">';
		echo '<h5>Files in queue ('.$entries.'):</h5><br>';
		echo '<ul>';
		echo $entrieshtml;
		echo '</ul>';
		echo '</blockquote>';
	}
	else
	{
		echo '';
	}
}

if($_GET["data"] === 'processed')
{
	$entries = 0;
	$entriesArray=array();
	$entrieshtml = '';
	$user_prefix = $_SESSION['userid'] . '_';
	
	
	// Populate array with all files in OUT folder //
	
	if ($handle = opendir($downloadBaseURI))
	{
		
		while (false !== ($entry = readdir($handle)))
		{
			if ($entry != "." && $entry != ".." && substr($entry, 0, strlen($user_prefix)) === $user_prefix)
			{
				array_push($entriesArray,$entry); // add file name including yara/sha256 files in array
			}
		}

		closedir($handle);
	}
	
	
	// Count actual number files processed //
	
	for ($i = 0; $i < count($entriesArray); $i++)
	{
		if(substr($entriesArray[$i], -7) == '.sha256') // .sha256 file found = one more file processed
		{
			$entries++;
		}
	}
	
	
	
	// Generate HTML //
	
	for ($i = 0; $i < count($entriesArray); $i++)
	{
		if( substr($entriesArray[$i], -7) == '.sha256') // run every time a .sha256 (=new file processed) is found
		{
			$user_prefix = $_SESSION['userid'] . '_';
			$fileSHA256 = file_get_contents($downloadBaseURI . $entriesArray[$i]); // read .sha256 file content
			$filename = substr($entriesArray[$i], 0, -7); // get original filename
			$clean_filename = substr($filename, strlen($user_prefix)); // remove id_ at beginning of filename for clean name
			$yarafilename = $filename . '.yara';
			
			
			if( in_array($filename, $entriesArray) && !in_array($yarafilename, $entriesArray) ) // the file is available in OUT folder, and no .yara file exists (clean file)
			{
				
				$entrieshtml.='<li style="line-height: 1em; margin-bottom:10px;"><span class="lnr lnr-checkmark-circle text-success"></span><a style="padding-left:4px;" class="text-success" href="' . $downloadBaseURL . $clean_filename . '" download>' . $clean_filename . '</a> <br><small>SHA256:<em style="padding-left:3px;">' . $fileSHA256 . '</em></small></li>';
			}
			
			if( in_array($filename, $entriesArray) && in_array($yarafilename, $entriesArray) ) // a file with .yara file
			{
				$yaraString = '';
				$yaraFoundId=0;
				$fn = fopen($downloadBaseURI . $yarafilename,"r");

				while(! feof($fn))
				{
					$yaraFoundId++;
					if($yaraFoundId > 1) {$yaraString.=', ';}
					$result = fgets($fn);
					$yaraString.= $result;
				}

				fclose($fn);
				
				
				$entrieshtml.='<li style="line-height: 1em; margin-bottom:10px;"><span class="lnr lnr-warning text-danger"></span><a style="padding-left:4px;" class="text-danger" href="' . $downloadBaseURL . $clean_filename . '" download>' . $clean_filename . '</a><br><small><span class="text-danger">The following patterns have been detected: '. $yaraString . '</span><br>SHA256:<em style="padding-left:3px;">' . $fileSHA256 . '</em></small></li>';
			}
			
			if( !in_array($filename, $entriesArray) && in_array($yarafilename, $entriesArray) ) // .yara file without corresponding file (deleted by AV)
			{
				$yaraString = '';
				$yaraFoundId=0;
				$fn = fopen($downloadBaseURI . $yarafilename,"r");

				while(! feof($fn))
				{
					$yaraFoundId++;
					if($yaraFoundId > 1) {$yaraString.=', ';}
					$result = fgets($fn);
					$yaraString.= $result;
				}

				fclose($fn);
				
				
				$entrieshtml.='<li style="line-height: 1em; margin-bottom:10px;"><span class="lnr lnr-warning text-danger"></span><a style="padding-left:4px;" class="text-danger">' . $clean_filename . ' <small>(deleted)</small></a><br><small><span class="text-danger">The following patterns have been detected: '. $yaraString . '</span><br>SHA256:<em style="padding-left:3px;">a' . $fileSHA256 . '</em></small></li>';
		
			}
			
			
		}
	} // end for
	
	
	if($entries > 0)
	{
		echo '<blockquote class="generic-blockquote" style="margin-bottom: 50px;">';
		echo '<h5>Files processed ('.$entries.'):</h5><br>';
		echo '<ul>';
		echo $entrieshtml;
		echo '</ul>';
		echo '</blockquote>';
	}
	else
	{
		echo '';
	}
}
		

?>